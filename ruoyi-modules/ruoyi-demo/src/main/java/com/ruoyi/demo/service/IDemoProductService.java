package com.ruoyi.demo.service;

import java.util.List;
import com.ruoyi.demo.domain.DemoProduct;

/**
 * 产品树表（mb）Service接口
 * 
 * @author 数据小王子
 * 2023-07-11
 */
public interface IDemoProductService 
{
    /**
     * 查询产品树表（mb）
     * 
     * @param productId 产品树表（mb）主键
     * @return 产品树表（mb）
     */
    DemoProduct selectDemoProductByProductId(Long productId);

    /**
     * 查询产品树表（mb）列表
     * 
     * @param demoProduct 产品树表（mb）
     * @return 产品树表（mb）集合
     */
    List<DemoProduct> selectDemoProductList(DemoProduct demoProduct);

    /**
     * 新增产品树表（mb）
     * 
     * @param demoProduct 产品树表（mb）
     * @return 结果
     */
    int insertDemoProduct(DemoProduct demoProduct);

    /**
     * 修改产品树表（mb）
     * 
     * @param demoProduct 产品树表（mb）
     * @return 结果
     */
    int updateDemoProduct(DemoProduct demoProduct);

    /**
     * 批量删除产品树表（mb）
     * 
     * @param productIds 需要删除的产品树表（mb）主键集合
     * @return 结果
     */
    int deleteDemoProductByProductIds(Long[] productIds);

    /**
     * 删除产品树表（mb）信息
     * 
     * @param productId 产品树表（mb）主键
     * @return 结果
     */
    int deleteDemoProductByProductId(Long productId);
}
